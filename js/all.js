jQuery(function($){

    var pull 		= $('#pull');
    menu 		= $('.small_menu ul');
    menuHeight	= menu.height();

    $(pull).on('click', function(e) {
        e.preventDefault();
        menu.slideToggle();
    });

    $(window).resize(function(){
        var w = $(window).width();
        if(w > 320 && menu.is(':hidden')) {
            menu.removeAttr('style');
        }
    });


    $('.bxslider').bxSlider({
        nextText: "",
        prevText: "",
        speed: 1000,
        auto: true,
        pause: 10000,
        autoHover: true
    });


    if ($(window).width() >= 960) {
        $('.animate-box').addClass("hidden").viewportChecker({
            classToAdd: 'visible animated fadeInDown', // Class to add to the elements when they are visible
            offset: 200
        });

        $('.animate-box2').addClass("hidden").viewportChecker({
            classToAdd: 'visible animated flipInX', // Class to add to the elements when they are visible
            offset: 200
        });

        $('.animate-box3').addClass("hidden").viewportChecker({
            classToAdd: 'visible animated fadeInUp', // Class to add to the elements when they are visible
            offset: 50
        });
    }



});

google.maps.event.addDomListener(window, 'load', init);
var map;
function init() {
    var mapOptions = {
        center: new google.maps.LatLng(55.651332,37.48288),
        zoom: 15,
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.SMALL,
        },
        disableDoubleClickZoom: false,
        mapTypeControl: true,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
        },
        scaleControl: true,
        scrollwheel: false,
        panControl: false,
        streetViewControl: true,
        draggable : true,
        overviewMapControl: false,
        overviewMapControlOptions: {
            opened: false,
        },
        mapTypeId: google.maps.MapTypeId.ROADMAP,
    }
    var mapElement = document.getElementById('map');
    var map = new google.maps.Map(mapElement, mapOptions);
    var locations = [
        ['Медицинский центр "СалютМЕД"', 'undefined', 'undefined', 'undefined', 'undefined', 55.651332,  37.48288, 'http://i.imgur.com/Fqjk4VQ.png']
    ];
    for (i = 0; i < locations.length; i++) {
        if (locations[i][1] =='undefined'){ description ='';} else { description = locations[i][1];}
        if (locations[i][2] =='undefined'){ telephone ='';} else { telephone = locations[i][2];}
        if (locations[i][3] =='undefined'){ email ='';} else { email = locations[i][3];}
        if (locations[i][4] =='undefined'){ web ='';} else { web = locations[i][4];}
        if (locations[i][7] =='undefined'){ markericon ='';} else { markericon = locations[i][7];}
        marker = new google.maps.Marker({
            icon: markericon,
            position: new google.maps.LatLng(locations[i][5], locations[i][6]),
            map: map,
            title: locations[i][0],
            desc: description,
            tel: telephone,
            email: email,
            web: web
        });
        link = '';     }





    jQuery.fn.exists = function() {
        return jQuery(this).length;
    }

    $(function() {

        if(!is_mobile()){

            if( $('.phonefield').exists()){
                $('.phonefield').mask('9 (999) 999-99-99');
            }

            if( $('.form_check').exists()){

                $('.form_check').each(function(){

                    var form = $(this),
                        btn = form.find('.btnsubmit');

                    form.find('.rfield').addClass('empty_field').parents('.rline').append('<span class="rfield_error">Заполните это поле</span>');
                    btn.addClass('disabled');

                    // Функция проверки полей формы
                    function checkInput(){

                        form.find('.rfield').each(function(){

                            if($(this).hasClass('phonefield')){

                                var pmc = $(this);
                                if ( (pmc.val().indexOf("_") != -1) || pmc.val() == '' ) {
                                    pmc.addClass('empty_field');
                                } else {
                                    pmc.removeClass('empty_field');
                                }

                            } else if($(this).hasClass('mailfield')) {

                                var mailfield = $(this);
                                var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
                                if(pattern.test(mailfield.val())){
                                    mailfield.removeClass('empty_field');
                                } else {
                                    mailfield.addClass('empty_field');
                                }

                            } else if($(this).is(':checkbox')) {
                                var checkBox = $(this);
                                if(checkBox.is(':checked')){
                                    checkBox.removeClass('empty_field')
                                } else {
                                    checkBox.addClass('empty_field')
                                }
                            } else if($(this).val() != '') {

                                $(this).removeClass('empty_field');

                            } else {

                                $(this).addClass('empty_field');
                            }

                        });
                    }

                    // Функция подсветки незаполненных полей
                    function lightEmpty(){
                        form.find('.empty_field').addClass('rf_error');
                        form.find('.empty_field').parents('.rline').find('.rfield_error').css({'visibility':'visible'});
                        setTimeout(function(){
                            form.find('.empty_field').removeClass('rf_error');
                            form.find('.empty_field').parents('.rline').find('.rfield_error').css({'visibility':'hidden'});
                        },1000);
                    }

                    //  Полсекундная проверка
                    setInterval(function(){
                        checkInput();
                        var sizeEmpty = form.find('.empty_field').length;
                        if(sizeEmpty > 0){
                            if(btn.hasClass('disabled')){
                                return false
                            } else {
                                btn.addClass('disabled')
                            }
                        } else {
                            btn.removeClass('disabled')
                        }
                    },10);

                    //  Клик по кнопке
                    btn.click(function(){
                        if($(this).hasClass('disabled')){
                            lightEmpty();
                            return false
                        } else {
                            form.submit();
                        }
                    });

                });

            }
        }

    });
}